var express = require('express'),
    jwt     = require('express-jwt'),
    requestjson = require('request-json'),
    config  = require('./config'),
    quoter  = require('./quoter');

var app = module.exports = express.Router();

// Validate access_token
var jwtCheck = jwt({
  secret: config.secret,
  audience: config.audience,
  issuer: config.issuer
});

// Check for scope
function requireScope(scope) {
  return function (req, res, next) {
    var has_scopes = req.user.scope === scope;
    if (!has_scopes) {
        res.sendStatus(401);
        return;
    }
    next();
  };
}

app.use('/api/protected', jwtCheck, requireScope('full_access'));

app.get('/api/protected/random-quote', function(req, res) {
  res.status(200).send(quoter.getRandomOne());
});

// Peticion consulta de movimientos con query por email
app.get('/api/protected/Movimientosq', function(req,res) {
  var query = 'q={"email":"'+ req.query.email+'"}';
  var urlMlabRaiz = "https://api.mlab.com/api/1/databases/rgarciab/collections/";
  var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var clienteMlabRaiz;

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Clientes?" + apiKey + "&"+ query);
  clienteMlabRaiz.get('')
    .then(function(response) {
      res.json(response.body);
    })
    .catch(function(error) {
      // En caso de error
      console.log(error);
    });
})

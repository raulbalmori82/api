var logger          = require('morgan'),
    cors            = require('cors'),
    http            = require('http'),
    express         = require('express'),
    errorhandler    = require('errorhandler'),
    dotenv          = require('dotenv'),
    bodyparser      = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json());
app.use(cors());

app.use(function(err, req, res, next) {
  if (err.name === 'StatusError') {
    res.send(err.status, err.message);
  } else {
    next(err);
  }
});

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
  app.use(errorhandler())
}

app.use(require('./anonymous-routes'));
app.use(require('./protected-routes'));
app.use(require('./user-routes'));
app.use(require('./protected2-routes'));


http.createServer(app).listen(port, function (err) {
  console.log('All ready in RESTful API server listening on http://localhost:' + port);
});

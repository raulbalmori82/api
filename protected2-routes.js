var express = require('express'),
    path = require('path'),
    requestjson = require('request-json'),
    bodyparser = require('body-parser'),
    bcrypt = require('bcryptjs'),
    movimientosJSON = require('./movimientosv2.json');

var app = module.exports = express.Router();
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/rgarciab/collections/";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var BCRYPT_SALT_ROUNDS = 12;

// Peticion GET
app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname,'index.html'));
})

// Peticion POST
app.post('/', function(req, res) {
  res.send('Hola hemos recibido su peticion post cambiada');
})

// Peticion PUT
app.put('/', function(req, res) {
  res.send('Hola hemos recibido su peticion put');
})

// Peticion DELETE
app.delete('/', function(req, res) {
  res.send('Hola hemos recibido su peticion delete');
})

// Consulta de movimientos por idCliente
app.get('/Clientes/:idcliente', function(req,res) {
  res.send('Aqui tiene al cliente número '+ req.params.idcliente);
})

// Consulta de movimientos regresando un archivo json
app.get('/Movimientos', function(req,res) {
  res.sendfile('movimientosv1.json');
})

// Consulta de movimientos regresando un archivo json en respuesta
app.get('/v2/Movimientos', function(req,res) {
  res.json(movimientosJSON);
})

// Consulta de movimiento por id
app.get('/v2/Movimientos/:id', function(req,res) {
  console.log(req.params.id-1);
  res.send(movimientosJSON[req.params.id-1]);
})

// Peticion consulta de movimientos con query
app.get('/v2/Movimientosq', function(req,res) {
  console.log(req.query);
  res.send('Query recibido '+ req.query);
})

// Inserta nuevo movimiento
app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})

// Login de ejemplo en clase
app.post('/v1/login', function(req, res){
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+ email +'","password":"'+ password + '"}';
  console.log(query);

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios?" + apiKey + "&"+ query);

  //console.log(urlMlabRaiz + "Usuarios?" + apiKey + "&"+ query);

  clienteMlabRaiz.get('', function(err,resM,body){
    if(!err){
        if(body.length == 1){   // login OK
            res.status(200).send('Usuario logado');
        } else {
            res.status(404).send('Usuario inexistente');
        }
    }
  });
})

// Insercion por peticion POST en Movimientos
app.post('/v2/Clientes', function(req, res){
  var clienteMlab = requestjson.createClient(urlMlabRaiz + "Clientes?"+ apiKey);
  clienteMlab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})

// Consulta de movimientos de Clientes
app.get ('/v2/Clientes',function (req, res) {
  var clienteMlab = requestjson.createClient(urlMlabRaiz + "Clientes?"+ apiKey);
  clienteMlab.get('')
    .then(function(response) {
      res.json(response.body);
    })
    .catch(function(error) {
      // En caso de error
      console.log(error);
    });
})

// Consulta de movimientos por idCliente
app.get('/v2/Clientes/:idcliente', function(req,res) {
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Clientes/"+req.params.idcliente+"?" + apiKey);
  clienteMlabRaiz.get('')
    .then(function(response) {
      res.json(response.body);
    })
    .catch(function(error) {
      // En caso de error
      console.log(error);
    });
})

var express = require('express'),
    _       = require('lodash'),
    config  = require('./config'),
    jwt     = require('jsonwebtoken'),
    requestjson = require('request-json'),
    bodyparser = require('body-parser'),
    bcrypt = require('bcryptjs');

var app = module.exports = express.Router();
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/rgarciab/collections/";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var BCRYPT_SALT_ROUNDS = 12;
var clienteMlabRaiz;

function createIdToken(user) {
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: '1h' });
}

function createAccessToken() {
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, config.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return jti;
}

function getUserScheme(req) {

  var username;
  var type;
  var userSearch = {};

  // The POST contains a username and not an email
  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }
  // The POST contains an email and not an username
  else if(req.body.email) {
    username = req.body.email;
    type = 'email';
    userSearch = { email: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

// Registro de usuario por peticion POST
app.post('/users', function(req, res) {

  var userScheme = getUserScheme(req);
  var password = req.body.password;

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("Debe enviar el usuario y contraseña");
  }

  var query = 'q={"email":"'+ userScheme.username+'"}';
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios?" + apiKey + "&"+ query);
  clienteMlabRaiz.get('')
    .then(function(result) {
      // Busca si el usuario existe en la BD
      return (result.body.length != 0);
    })
    .then( function(userExist){
      if(!userExist){
          // Si no esta en BD encripta el password
          return bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
      } else {
          // El usuario existe en BD regresa Error
          throw new Error('El usuario ya existe');
      }
    })
    .then(function(hashedPassword) {
      // Crea json con password encriptado
        var data = {
          email: userScheme.username,
          password: hashedPassword
        };
        return data;
    })
    .then(function(dataHashed) {
        clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios?" + apiKey);
        // Inserta en la BD el nuevo usuario
        clienteMlabRaiz.post('', dataHashed, function(err, resM, body) {
          res.status(201).send({
            id_token: createIdToken(userScheme.username),
            access_token: createAccessToken()
          });
        });
    })
    .catch(function(error){
        // En caso de algun Error
        var mensajeError = 'El usuario ya existe';
        console.log(mensajeError +' '+ error);
        res.status(404).send(mensajeError);
    });
});

// Login de usuario por peticion POST
app.post('/sessions/create', function(req, res) {

  var userScheme = getUserScheme(req);
  var password = req.body.password;

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("Debe enviar el usuario y contraseña");
  }

  var query = 'q={"email":"'+ userScheme.username+'"}';
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios?" + apiKey + "&"+ query);
  // Busca el usuario en la BD
  clienteMlabRaiz.get('')
    .then(function(result) {
      usuario = result.body[0];
      // En caso de existir en BD compara el password encriptado
      return bcrypt.compare(password, usuario.password);
    })
    .then(function(samePassword){
      // Revisa el resultado de comparacion de password
      if(!samePassword) {
        return res.status(401).send('Password incorrecto');
      } else {
        return res.status(201).send({
          id_token: createIdToken(usuario),
          access_token: createAccessToken()
        });
      }
    })
    .catch(function(error) {
      // En caso de error
      console.log(error);
      return res.status(401).send('Usuario inexistente');
    });
});

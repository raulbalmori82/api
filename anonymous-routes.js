var express = require('express'),
    requestjson = require('request-json'),
    quoter  = require('./quoter');

var app = module.exports = express.Router();


app.get('/api/random-quote', function(req, res) {
  res.status(200).send(quoter.getRandomOne());
});

// Llamada a API externa
app.get('/api/exchange-rates', function(req,res) {
  var urlFixer = "http://data.fixer.io/api/latest?access_key=262eb3bfb968b99de257cc62a090c05d&symbols=USD,AUD,CAD,PLN,MXN&format=1";
  var cliente = requestjson.createClient(urlFixer);
  // Peticion GET a Fixer
  cliente.get('')
    .then(function(response) {
      res.json(response.body);
    })
    .catch(function(error) {
      console.log(error);
    });
});

// Peticion consulta de movimientos con query por email
app.get('/api/Movimientosq', function(req,res) {
  var query = 'q={"email":"'+ req.query.email+'"}';
  var urlMlabRaiz = "https://api.mlab.com/api/1/databases/rgarciab/collections/";
  var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
  var clienteMlabRaiz;

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Clientes?" + apiKey + "&"+ query);
  clienteMlabRaiz.get('')
    .then(function(response) {
      res.json(response.body);
    })
    .catch(function(error) {
      // En caso de error
      console.log(error);
    });
})
